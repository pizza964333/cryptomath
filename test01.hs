{-# LANGUAGE DataKinds #-}

import System.Random
import Data.CryptoMath.EllipticCurve
import Data.CryptoMath.GaussianPrimeField
import System.IO.Unsafe
import Data.List

random01 :: IO (ECPF 10 100 1009)
random01 = do
  x <- randomRIO (-1000,1000)
  y <- randomRIO (-1000,1000)
  return $ ECP (fromInteger x) (fromInteger y)

test = do
  a <- random01
  let check = all isPointOnCurve $ concatMap toList $ [Just a, pointAddition a a]
  b <- unsafeInterleaveIO $ test
  if check then return (a:b) else test

toList (Just a) = [a]
toList _ = []

curves :: [ECPF 10 100 1009]
curves = [ECP {x = 294, y = 358},ECP {x = 397, y = 575},ECP {x = 397, y = 434},ECP {x = 672, y = 1006},ECP {x = 672, y = 3},ECP {x = 294, y = 651}]

make list = nub $ filter isPointOnCurve $ concat $ [ concatMap toList [Just a, Just b, pointAddition a b] | a <- list, b <- list ]

curves2 = fst $ countAndTake $ groupBy (\(_,a) (_,b) -> a==b) $ map (\a -> (a,length a)) $ iterate make curves
 where
  single [a] = True
  single _ = False
  countAndTake a = head $ a !! (length $ takeWhile single a)

