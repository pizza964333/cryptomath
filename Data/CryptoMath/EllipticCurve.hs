{-# LANGUAGE FlexibleContexts, UndecidableInstances, DataKinds, KindSignatures, OverloadedLabels, ScopedTypeVariables, DatatypeContexts, PackageImports, MultiParamTypeClasses, FlexibleInstances, RankNTypes, TypeInType #-}

module Data.CryptoMath.EllipticCurve where

import GHC.TypeLits
import Data.Proxy
import Data.Ratio
import Data.CryptoMath.PrimeField
import Data.CryptoMath.GaussianPrimeField (GPF)

test01 :: ECPF 10 10 137
test01 = ECP 123 456 

test02 :: ECR 20 20
test02 = ECP 12 13

type ECR  a b   = EC a b Rational
type ECPF a b p = EC a b (PF p)
type ECGPF a b c d = EC a b (GPF c d)

type ECGPF01 = ECGPF 20 20 27 20

type IntegerAsNat = Nat

data EC (a :: IntegerAsNat) (b :: IntegerAsNat) v = ECP { x :: v, y :: v }
 deriving (Show,Eq)

isPointOnCurve :: (KnownNat a, KnownNat b, Eq v, Fractional v) => EC a b v -> Bool
isPointOnCurve o@(ECP x y) = check
 where
  a = getParameterA o
  b = getParameterB o
  check = y*y == x*x*x + a*x + b

{-# SPECIALIZE pointAddition :: (KnownNat a, KnownNat b) => ECR a b -> ECR a b -> Maybe (ECR a b) #-}
{-# SPECIALIZE pointAddition :: (KnownNat a, KnownNat b, KnownNat p) => ECPF a b p -> ECPF a b p -> Maybe (ECPF a b p) #-}
pointAddition :: (KnownNat a, KnownNat b, Eq v, Fractional v) => EC a b v -> EC a b v -> Maybe (EC a b v)
pointAddition o@(ECP x1 y1) u@(ECP x2 y2)
  | y1 /= y2 && x1 == x2 = Nothing
  | otherwise            = Just $ ECP x3 y3
 where
  a   = getParameterA o
  b   = getParameterB o
  lam | o == u    = (x1*x1*3+a) / (y1*2)
      | otherwise = (y1 - y2) / (x1 - x2)
  x3 = lam*lam - x1 - x2
  y3 = lam * (x1 - x2) - y1

getParameterA :: (KnownNat a, Fractional v) => EC a b v -> v
getParameterA = realToFrac . natToInteger . natVal . proxy
 where
  proxy :: EC a b v -> Proxy a
  proxy = undefined

getParameterB :: (KnownNat b, Fractional v) => EC a b v -> v
getParameterB = realToFrac . natToInteger . natVal . proxy
 where
  proxy :: EC a b v -> Proxy b
  proxy = undefined

natToInteger :: Integer -> Integer
natToInteger a = c*e
 where
  b = a + 1
  c = b`div`2
  d = b`mod`2
  e = d*2-1

