{-# LANGUAGE FlexibleContexts, UndecidableInstances, DataKinds, KindSignatures, OverloadedLabels, ScopedTypeVariables, DatatypeContexts, PackageImports, MultiParamTypeClasses, FlexibleInstances, CPP #-}

module Data.CryptoMath.PrimeField where

import GHC.TypeLits
import Data.Bits
import Data.Maybe
import Data.Ratio

#if MIN_VERSION_integer_gmp(0,5,1)
import Prelude
#else
import Prelude hiding ((^))
#endif
import qualified Prelude as P
import "crypto-numbers" Crypto.Number.ModArithmetic (expFast,inverse)

newtype PF (prime :: Nat) = PF Integer

getPrime :: KnownNat prime => PF prime -> Integer
getPrime = natVal

instance KnownNat prime => Num (PF prime) where
  o@(PF a) + (PF b) = PF $ (a + b) `mod` getPrime o
  o@(PF a) - (PF b) = PF $ (a - b) `mod` getPrime o
  o@(PF a) * (PF b) = PF $ (a * b) `mod` getPrime o
  negate o@(PF a) = PF $ getPrime o - a
  fromInteger a = b where b = PF $ a `mod` getPrime b

instance KnownNat prime => Show (PF prime) where
  show o@(PF a) = show $ a `mod` getPrime o

instance KnownNat prime => Bits (PF prime) where
  shiftL o@(PF a) n = PF $ shiftL a n `mod` getPrime o

instance KnownNat prime => Eq (PF prime) where
  (PF a) == (PF b) = a == b

instance KnownNat prime => Fractional (PF prime) where
  o@(PF a) / (PF b) = o * (PF $ fromJust $ inverse b $ getPrime o)
  fromRational a = (fromInteger $ numerator a) / (fromInteger $ denominator a)

{-# SPECIALIZE (^) :: KnownNat prime => PF prime -> Integer -> PF prime #-}

{-# SPECIALIZE dbl :: KnownNat prime => PF prime -> PF prime #-}
dbl a = shiftL a 1

#if MIN_VERSION_integer_gmp(0,5,1)
#else
class Num a => Power a where
  (^) :: Integral b => a -> b -> a

instance {-# OVERLAPPABLE #-} Num a => Power a where
  a ^ b = (P.^) a b

instance {-# OVERLAPPING #-} KnownNat prime => Power (PF prime) where
  o@(PF a) ^ b = PF $ expFast a (toInteger b) (getPrime o)
#endif




















































testA = fromInteger 12312312 :: PF 127
testB = fromInteger 12312 :: PF 127
testC = fromInteger 123123221 :: PF 179426549
testE = testA ^ 1232131





