module Data.CryptoMath.GaussianInteger where

import System.IO.Unsafe
import Data.Complex
import Math.NumberTheory.Primes.Factorisation
import System.Random
import Prelude hiding (divMod)
import qualified Prelude as P
import Data.Ratio

type GaussPrime = ComplexPF
data ComplexPF = CPF { real :: Integer, imag :: Integer }
 deriving (Show,Eq)

add :: ComplexPF -> ComplexPF -> ComplexPF
add (CPF a b) (CPF c d) = CPF (a+c) (b+d)

mul :: ComplexPF -> ComplexPF -> ComplexPF
mul (CPF a b) (CPF c d) = CPF g h
 where
  g = a*c - b*d
  h = b*c + a*d

pMul :: GaussPrime -> ComplexPF -> ComplexPF -> ComplexPF
pMul p a b = normM p $ mul (normM p a) (normM p b)

pAdd :: GaussPrime -> ComplexPF -> ComplexPF -> ComplexPF
pAdd p a b = normA p $ add a b

normM p a = snd $ a `divMod` p

normA (CPF a b) (CPF c d) = CPF (c`mod`prime) (d`mod`prime)
 where
  prime = a*a + b*b

divMod :: ComplexPF -> ComplexPF -> (ComplexPF, ComplexPF)
divMod (CPF a b) (CPF c d) = (CPF r t, CPF s u)
 where
  g = a*c + b*d
  h = b*c - a*d
  k = c*c + d*d -- k is prime if gaussian prime
  (r,s) = P.divMod g k
  (t,u) = P.divMod h k

--inverseP :: ComplexPF -> ComplexPF -> ComplexPF
--inverseP gpf = CPF 1 0 

randomGaussPrime :: IO ComplexPF
randomGaussPrime = do
  a <- randomRIO (1,999 :: Integer)
  b <- randomRIO (1,999 :: Integer)
  let c = a*a + b*b
  let d = factorise c
  case (d,((fst $ head d)-3)`mod`4) of
    (_,0)       -> randomGaussPrime
    ([(n,1)],_) | test n -> do print (n,j n) ; return $ CPF a b
    _           -> randomGaussPrime
 where
   j n = (log(realToFrac n) / log 2)
   test n = let a = (log(realToFrac n) / log 2) in a > 15.9996 && a < 16

randomGaussInteger :: IO ComplexPF
randomGaussInteger = do
  a <- randomRIO (-9999,9999 :: Integer)
  b <- randomRIO (-9999,9999 :: Integer)
  return $ CPF a b

  


