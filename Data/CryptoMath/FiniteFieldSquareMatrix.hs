{-# LANGUAGE FlexibleContexts, UndecidableInstances, DataKinds, KindSignatures, OverloadedLabels, ScopedTypeVariables, DatatypeContexts, PackageImports, MultiParamTypeClasses, FlexibleInstances, GADTs, TypeOperators, TypeFamilies #-}

module Data.CryptoMath.FiniteFieldSquareMatrix where

import Data.List
import GHC.TypeLits
import GHC.Prim
import Data.CryptoMath.GaussianPrimeField

type GPF16 = GPF 240 89
type SQMTGPF16 = SQMT 4 GPF16

randomSQMTGPF16 :: IO SQMTGPF16
randomSQMTGPF16 = do
  a <- mapM (\_ -> randomGPF) [0..15]
  let e n = a !! n
  return $ SQMT $ e 0 :. e 1 :. e 2 :. e 3 :. e 4 :. e 5 :. e 6 :. e 7 :. e 8 :. e 9 :. e 10 :. e 11 :. e 12 :. e 13 :. e 14 :. e 15 :. ()

data a :. b = a :. b
 deriving (Show)

infixr :.

type family TT a b where
  TT 0 a = ()
  TT n a = a :. TT (n-1) a

test :: TT 16 GPF16
test = undefined

test2 :: GPF16 :. GPF16 :. GPF16 :. ()
test2 = undefined

data SQMT (size :: Nat) a where
     SQMT :: TT (size * size) a -> SQMT size a

instance Show SQMTGPF16 where
  show (SQMT a) = show a

--test2 :: Num c => MT a b c -> MT d e c -> MT f g c
--test2 = undefined

{-
data SQM  (size :: Nat) a where
     SQM2 :: 
-}

mmult :: Num a => [[a]] -> [[a]] -> [[a]] 
mmult a b = [[ sum $ zipWith (*) ar bc | bc <- (transpose b)] | ar <- a ]

