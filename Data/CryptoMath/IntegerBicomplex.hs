module Data.Cryptomath.IntegerBicomplex where

mul (BCF w1 i1 j1 k1) (BCF w2 i2 j2 k2)

{-

  *                                       *                                       *                                       *
w1*w2 + w1*i2 + w1*j2 + w1*k2 + i1*w2 + i1*i2 + i1*j2 + i1*k2 + j1*w2 + j1*i2 + j1*j2 + j1*k2 + k1*w1 + k1*i2 + k1*j2 + k1*k2
w       i       j       k       i       -w      k       -j      j       k       w       i       k       -j      i       -w
+       -       -       -       +        -      -        -      +       -       -       -       +        -      -        -
w      -i      -j      -k       i        w     -k        j      j      -k      -w      -i       k        j     -i        w
        A                       A
                B                                               B
                        C                                                                       C
                                                E                       E
                                                         F                                               F
                                                                                        D                       D 

(w1 + i1 + j1 + k1) * (w2 - i2 - j2 - k2)
-----------------------------------------
num(w2*w2 + i2*i2 - j2*j2 + k2*k2)


-}
