{-# LANGUAGE FlexibleContexts, UndecidableInstances, DataKinds, KindSignatures, OverloadedLabels, ScopedTypeVariables, DatatypeContexts, PackageImports, MultiParamTypeClasses, FlexibleInstances, GADTs, TypeOperators, TypeFamilies, FunctionalDependencies #-}

module Data.CryptoMath.UnboxedTypedVector where

import Data.List
import GHC.TypeLits
import GHC.Prim
import Data.Word
import Text.Printf

data a :. b = a :. b

infixr 8 :.

data UTVI a = UTVI

type UTV a b = UTVF a b a b

type family UTVF a b c d where
  UTVF  0    b  0    d   =           UTVI b
  UTVF 64 Bool 64 Bool   =           UTVF   64     Bool                     1  Word64
  UTVF  a Bool  a Bool   =           UTVF    a     Bool  (Div (2 ^ Log2 a) 64) Word64
  UTVF  a    b  a    b   = b      :. UTVF (a-1)       b                  (a-1)      b
  UTVF  a Bool  c Word64 = Word64 :. UTVF (a - 64) Bool                (c - 1) Word64

class FromList a b where
  fromList :: [a] -> b

instance {-# OVERLAPPABLE #-} FromList a (UTVI a) where
  fromList [] = UTVI

instance {-# OVERLAPPING #-} FromList Word64 (UTVI Bool) where
  fromList [] = UTVI

instance FromList Word64 (Word64 :. UTVI Bool) where
  fromList [a] = a :. UTVI

instance FromList Word64 (Word64 :. a) => FromList Word64 (Word64 :. Word64 :. a) where
  fromList (a:b) = a :. fromList b

instance Show (Word64 :. UTVI Bool) where
  show (a :. _) = printf "0x%016X0\n" a

instance Show (Word64 :. a) => Show (Word64 :. Word64 :. a) where
  show (a :. b) = printf "0x%016X0\n" a ++ show b

class GetSize a where
  getSize :: a -> Int

instance GetSize (UTVI a) where
  getSize _ = 0

instance GetSize b => GetSize (a :. b) where
  getSize (a :. b) = getSize b + 1


--fromList [] = UTVI

test :: UTV 256 Bool
test = undefined


