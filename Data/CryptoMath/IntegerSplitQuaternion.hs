module Data.CryptoMath.IntegerSplitComplex where

import System.IO.Unsafe
import Data.Complex
import Math.NumberTheory.Primes.Factorisation
import System.Random
import Prelude hiding (divMod)
import qualified Prelude as P
import Data.Ratio

type SQPrime = SplitQPF
data SplitQPF = SQPF { real :: Integer, imagI :: Integer, imagJ :: Integer, imagK :: Integer }
 deriving (Show,Eq)

getPrimeFromSQP (SQPF a b c d) = a*a + b*b - c*c - d*d

sqPrime01 = SQPF {real = 907486928906263697595, imagI = 865802379462841186933, imagJ = 1056817267617986891470, imagK = 306780417551016359951}

pAdd :: SQPrime -> SplitQPF -> SplitQPF -> SplitQPF
pAdd p a b = normA p $ add a b

normA (SQPF a b c d) (SQPF e f g h) = SQPF (e`mod`prime) (f`mod`prime) (g`mod`prime) (g`mod`prime)
 where
  prime = a*a + b*b - c*c - d*d

add :: SplitQPF -> SplitQPF -> SplitQPF
add (SQPF w1 i1 j1 k1) (SQPF w2 i2 j2 k2) = SQPF (w1+w2) (i1+i2) (j1+j2) (k1+k2)

mul :: SplitQPF -> SplitQPF -> SplitQPF
mul (SQPF w1 i1 j1 k1) (SQPF w2 i2 j2 k2) = SQPF w i j k
 where
  w = w1*w2 - i1*i2 + j1*j2 + k1*k2
  i = w1*i2 + i1*w2 + j1*k2 - k1*j2
  j = w1*j2 + i1*k2 + j1*w2 - k1*i2
  k = w1*k2 - i1*j2 + j1*i2 + k1*w2

normM p a = snd $ a `divMod` p

-- pMul :: SCIPrime -> SplitCPF -> SplitCPF -> SplitCPF
pMul p a b = normM p $ mul (normM p a) (normM p b)

divMod :: SplitQPF -> SplitQPF -> (SplitQPF, SplitQPF)
divMod (SQPF w1 i1 j1 k1) (SQPF w2 i2 j2 k2) = (SQPF w3 i3 j3 k3, SQPF w4 i4 j4 k4)
 where
  w =   w1*w2  + i1*i2 - j1*j2 - k1*k2
  i = (-w1*i2) + i1*w2 - j1*k2 + k1*j2
  j = (-w1*j2) - i1*k2 + j1*w2 + k1*i2
  k = (-w1*k2) + i1*j2 - j1*i2 + k1*w2
  p = w2*w2 + i2*i2 - j2*j2 - k2*k2
  (w3,w4) = P.divMod w p
  (i3,i4) = P.divMod i p
  (j3,j4) = P.divMod j p
  (k3,k4) = P.divMod k p
  -- divMod a b = (a,b)

{-

  *                                       *                                       *                                       *
w1*w2 + w1*i2 + w1*j2 + w1*k2 + i1*w2 + i1*i2 + i1*j2 + i1*k2 + j1*w2 + j1*i2 + j1*j2 + j1*k2 + k1*w2 + k1*i2 + k1*j2 + k1*k2
w       i       j       k       i       -w      -k      j       j       k       w       i       k       -j      -i      w
+       -       -       -       +        -       -      -       +       -       -       -       +        -       -      -
w      -i      -j      -k       i        w       k     -j       j      -k      -w      -i       k        j       i     -w
        A                       A
                B                                               B
                        C                                                                       C
                                                 D                      D
                                                        E                                                E
                                                                                        F                        F
                           


(w1 + i1 + j1 + k1) * (+ w2 - i2 - j2 - k2)
------------------------------------------
num(w2*w2 + i2*i2 - j2*j2 - k2*k2)


-}

randomSQPrime :: IO SplitQPF
randomSQPrime = do
  let mx = 2^70
  a <- randomRIO (1,mx :: Integer)
  b <- randomRIO (1,mx :: Integer)
  c <- randomRIO (1,mx :: Integer)
  d <- randomRIO (1,mx :: Integer)
  let e = a*a + b*b - c*c - d*d
  let g = factorise e
  case (e,g,((fst $ head g)-3)`mod`4) of
  --case (e,g,0) of
    (0,_,_)       -> randomSQPrime
    (_,_,0)       -> randomSQPrime
    (_,[(n,1)],_) -> return $ SQPF a b c d
    _             -> randomSQPrime

randomSplitCI :: IO SplitQPF
randomSplitCI = do
  let mx = 2^70
  a <- randomRIO (-mx,mx :: Integer)
  b <- randomRIO (-mx,mx :: Integer)
  c <- randomRIO (-mx,mx :: Integer)
  d <- randomRIO (-mx,mx :: Integer)
  return $ SQPF a b c d

{-

-}
  


