{-# LANGUAGE FlexibleContexts, UndecidableInstances, DataKinds, KindSignatures, OverloadedLabels, ScopedTypeVariables, DatatypeContexts, PackageImports, MultiParamTypeClasses, FlexibleInstances, RankNTypes, TypeInType #-}

module Data.CryptoMath.TwistedEdwardsCurve where

import GHC.TypeLits
import Data.Proxy
import Data.Ratio
import Data.CryptoMath.PrimeField
import Data.CryptoMath.GaussianPrimeField (GPF)

type IntegerAsNat = Nat

data TWEDC (a :: IntegerAsNat) (d :: IntegerAsNat) v = TWEDCP { x :: v, y :: v }
 deriving (Show,Eq)

isPointOnCurve :: (KnownNat a, KnownNat b, Eq v, Fractional v) => TWEDC a b v -> Bool
isPointOnCurve o@(TWEDCP x y) = check
 where
  a = getParameterA o
  d = getParameterD o
  check = a*x*x + y*y == 1 + d*x*x*y*y

-- {-# SPECIALIZE pointAddition :: (KnownNat a, KnownNat b) => ECR a b -> ECR a b -> Maybe (ECR a b) #-}
-- {-# SPECIALIZE pointAddition :: (KnownNat a, KnownNat b, KnownNat p) => ECPF a b p -> ECPF a b p -> Maybe (ECPF a b p) #-}
pointAddition :: (KnownNat a, KnownNat d, Eq v, Fractional v) => TWEDC a d v -> TWEDC a d v -> Maybe (TWEDC a d v)
pointAddition o@(TWEDCP x1 y1) u@(TWEDCP x2 y2)
  | otherwise            = Just $ TWEDCP x3 y3
 where
  a   = getParameterA o
  d   = getParameterD o
  x3 = ( x1*y2+y1*x2 ) / ( 1 + d*x1*x2*y1*y2 )
  y3 = ( y1*y2 - a*x1*x2 ) / ( 1 - d*x1*x2*y1*y2 )

getParameterA :: (KnownNat a, Fractional v) => TWEDC a b v -> v
getParameterA = realToFrac . natToInteger . natVal . proxy
 where
  proxy :: TWEDC a b v -> Proxy a
  proxy = undefined

getParameterD :: (KnownNat b, Fractional v) => TWEDC a b v -> v
getParameterD = realToFrac . natToInteger . natVal . proxy
 where
  proxy :: TWEDC a b v -> Proxy b
  proxy = undefined

natToInteger :: Integer -> Integer
natToInteger a = c*e
 where
  b = a + 1
  c = b`div`2
  d = b`mod`2
  e = d*2-1

