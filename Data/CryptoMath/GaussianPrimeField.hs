{-# LANGUAGE FlexibleContexts, UndecidableInstances, DataKinds, KindSignatures, OverloadedLabels, ScopedTypeVariables, DatatypeContexts, PackageImports, MultiParamTypeClasses, FlexibleInstances, CPP #-}

module Data.CryptoMath.GaussianPrimeField where

import System.IO.Unsafe
import GHC.TypeLits
import Data.Bits
import Data.Maybe
import Data.Proxy
import Data.CryptoMath.GaussianInteger
import Prelude hiding (divMod)
import Data.List
import System.Random

newtype GPF (real :: Nat) (imag :: Nat) = GPF { gaussInteger :: ComplexPF }
  deriving (Show,Eq)

type GPF01 = GPF   8   7
type GPF02 = GPF  27  20
type GPF03 = GPF  96  49
type GPF04 = GPF 589  96
type GPF05 = GPF 566 519
type GPF06 = GPF 125 938

randomGPF :: (KnownNat a, KnownNat b) => IO (GPF a b)
randomGPF = do
  let a = fromInteger 0
  let b = getGPFPrime a
  c <- randomRIO (0,b-1)
  return (a + fromInteger c)

getGP :: (KnownNat real, KnownNat imag) => GPF real imag -> ComplexPF
getGP a = CPF (natVal $ proxy1 a) (natVal $ proxy2 a)
 where
  proxy1 :: KnownNat real => GPF real imag -> Proxy real
  proxy1 = undefined
  proxy2 :: KnownNat imag => GPF real imag -> Proxy imag
  proxy2 = undefined

getGPFPrime :: (KnownNat real, KnownNat imag) => GPF real imag -> Integer
getGPFPrime a = b*b + c*c
 where
  (CPF b c) = getGP a

findUnitMult a = group $ nub $ b
 where
  b = filter snd $ [ (b,(b * b) == b) | b <- a, c <- a ]

allValues :: (KnownNat real, KnownNat imag) => [GPF real imag]
allValues = a : map fromInteger [1..(b-1)]
 where
  a = 0
  b = getGPFPrime a

findLog a b = nub $ map snd $ filter ((==b).fst) $ map (\p -> (a^p,p)) $ [0..(getGPFPrime a * 10)]

slowGetIndex a = toInteger $ fromJust $ findIndex (==a) allValues

instance (KnownNat real, KnownNat imag) => Num (GPF real imag) where
  o@(GPF a) + (GPF b) = GPF $ pAdd (getGP o) a b
  o@(GPF a) * (GPF b) = GPF $ pMul (getGP o) a b
  --negate o@(PF a) = PF $ getPrime o - a
  fromInteger a = b where b = GPF $ normM (getGP b) $ CPF a 0

instance (KnownNat real, KnownNat imag) => Fractional (GPF real imag) where

{-
instance KnownNat prime => Show (PF prime) where
  show o@(PF a) = show $ a `mod` getPrime o

instance KnownNat prime => Bits (PF prime) where
  shiftL o@(PF a) n = PF $ shiftL a n `mod` getPrime o

instance KnownNat prime => Eq (PF prime) where
  (PF a) == (PF b) = a == b

instance KnownNat prime => Fractional (PF prime) where
  o@(PF a) / (PF b) = o * (PF $ fromJust $ inverse b $ getPrime o)
  fromRational a = (fromInteger $ numerator a) / (fromInteger $ denominator a)

{-# SPECIALIZE (^) :: KnownNat prime => PF prime -> Integer -> PF prime #-}

{-# SPECIALIZE dbl :: KnownNat prime => PF prime -> PF prime #-}
dbl a = shiftL a 1

#if MIN_VERSION_integer_gmp(0,5,1)
#else
class Num a => Power a where
  (^) :: Integral b => a -> b -> a

instance {-# OVERLAPPABLE #-} Num a => Power a where
  a ^ b = (P.^) a b

instance {-# OVERLAPPING #-} KnownNat prime => Power (PF prime) where
  o@(PF a) ^ b = PF $ expFast a (toInteger b) (getPrime o)
#endif




















































testA = fromInteger 12312312 :: PF 127
testB = fromInteger 12312 :: PF 127
testC = fromInteger 123123221 :: PF 179426549
testE = testA ^ 1232131
-}





