{-# LANGUAGE DataKinds, TypeOperators, KindSignatures, FlexibleContexts #-}

module Data.CryptoMath.BitMatrix where

import Data.CryptoMath.UnboxedTypedVector
import GHC.TypeLits
import System.Random
import Data.Word

data BM (size :: Nat) = BM (UTVF (size*size) Bool (size*size) Bool)

{-
instance KnownNat n => Show (BM n) where
  show (BM a) = show a
-}

type BM2 = BM 16

test2 (BM a) = a

test3 = test2 (undefined :: BM2)

randomBitMatrix :: IO BM2
randomBitMatrix = do
  let result list = fromList (list :: [Word64]) :: (Word64 :. Word64 :. Word64 :. Word64 :. UTVI Bool)
  let len = getSize (result [1,2,3,4])
  list <- mapM (\_ -> randomRIO (0 :: Word64,2^64-1)) [1..len]
  return $ BM $ result list


