{-# LANGUAGE FlexibleContexts, UndecidableInstances, DataKinds, KindSignatures, OverloadedLabels, ScopedTypeVariables, DatatypeContexts, PackageImports, MultiParamTypeClasses, FlexibleInstances, GADTs, TypeOperators, TypeFamilies #-}

module Data.CryptoMath.FiniteFieldMatrix where

import Data.List
import GHC.TypeLits
import GHC.Prim

-- data SQM a = SQM [[a]]


data a :. b = a :. b

type family TT a b where
  TT 0 a = ()
  TT n a = a :. TT (n-1) a

test :: TT 10 Int
test = undefined

data MT (width :: Nat) (height :: Nat) a where
     MT :: TT (width * height) a -> MT width height a

test2 :: Num c => MT a b c -> MT d e c -> MT f g c
test2 = undefined

{-
data SQM  (size :: Nat) a where
     SQM2 :: 
-}

mmult :: Num a => [[a]] -> [[a]] -> [[a]] 
mmult a b = [[ sum $ zipWith (*) ar bc | bc <- (transpose b)] | ar <- a ]

