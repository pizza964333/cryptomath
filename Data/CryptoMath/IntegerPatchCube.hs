module Data.CryptoMath.IntegerPatchCube where

import System.IO.Unsafe
import Data.Complex
import Math.NumberTheory.Primes.Factorisation
import System.Random
import Prelude hiding (divMod)
import qualified Prelude as P
import Data.Ratio

mul a@(CUP w1 x1 y1 z1) b@(CUP w2 x2 y2 z2) = undefined
 where
  wwM = w1*w2 ; wxL = w1*x2 ; wyL = w1*y2 ; wzL = w1*z2
  xxM = x1*x2 ; xwL = x1*w2 ; xyL = x1*y2 ; xzL = x1*z2
  ywL = y1*w2 ; yxL = y1*x2 ; yyM = y1*y2 ; yzL = y1*z2
  zwL = z1*w2 ; zxL = z1*x2 ; zyL = z1*y2 ; zzM = z1*z2

  
  

data CUP = CUP { qw :: Integer, qx :: Integer, qy :: Integer, qz :: Integer }
 deriving (Show)

mag (CUP w x y z) = (x+y+z)^2 + w*w - x*x - y*y - z*z

randomPrime :: IO [Integer]
randomPrime = do
  let mx = 2^16
  w <- randomRIO (1,mx :: Integer)
  x <- randomRIO (1,mx :: Integer)
  y <- randomRIO (1,mx :: Integer)
  z <- randomRIO (1,mx :: Integer)
  let e = ( (x+y+z)^2 + w*w - x*x - y*y - z*z )
  let g = factorise e
  --print (e,g)
  case (e > 2,g,((fst $ head g)-3)`mod`4) of
  --case (e,g,0) of
    (False,_,_)       -> randomPrime
    -- (_,_,0)       -> randomSQPrime
    (_,[(n,1)],_) -> return $ [w,x,y,z]
    _             -> randomPrime

randomCUP :: IO CUP
randomCUP = do
  let mx = (2^16) :: Integer
  w <- randomRIO (1,mx)
  x <- randomRIO (1,mx)
  y <- randomRIO (1,mx)
  z <- randomRIO (1,mx)
  mm <- randomRIO (1,1)
  let m = mm*2-1
  return $ CUP (w*m) (x*m) (y*m) (z*m)
  

{-

instance Num CUP where
  a * b = mul a b
  a + b = add a b

check (CUP x y z) = f (x>0) (y>0) (z>0)
 where
  f True True True = True
  f False False False = True
  f _ _ _ = False

add :: CUP -> CUP -> CUP
add (CUP x1 y1 z1) (CUP x2 y2 z2) = CUP (x1+x2) (y1+y2) (z1+z2)

{-
  ma = mag a
  mb = mag b
  pa = (x1+y1+z1)^2
  pb = (x2+y2+z2)^2
  qa = ma - pa
  qb = mb - pb
-}

{-

z = (1-2*x*y) / (2*(x+y))


-}

{-

-}

{-
  w = w1*w2 - i1*i2 + j1*j2 + k1*k2
  i = w1*i2 + i1*w2 + j1*k2 - k1*j2
  j = w1*j2 + i1*k2 + j1*w2 - k1*i2
  k = w1*k2 - i1*j2 + j1*i2 + k1*w2
-}

{-
ii  w
jj  w
kk  w
kj  i
ij  k
ik  j
-}


--pAdd :: [Integer] -> CUP -> CUP -> CUP
--pAdd [px,py,pz] (CUP x1 y1 z1) (CUP x2 y2 z2) =


{-
getPrimeFromSQP (SQPF a b c d) = a*a + b*b - c*c - d*d

type SQPrime = SplitQPF
data SplitQPF = SQPF { real :: Integer, imagI :: Integer, imagJ :: Integer, imagK :: Integer }
 deriving (Show,Eq)

sqPrime01 = SQPF {real = 907486928906263697595, imagI = 865802379462841186933, imagJ = 1056817267617986891470, imagK = 306780417551016359951}

pAdd :: SQPrime -> SplitQPF -> SplitQPF -> SplitQPF
pAdd p a b = normA p $ add a b

normA (SQPF a b c d) (SQPF e f g h) = SQPF (e`mod`prime) (f`mod`prime) (g`mod`prime) (g`mod`prime)
 where
  prime = a*a + b*b - c*c - d*d

add :: SplitQPF -> SplitQPF -> SplitQPF
add (SQPF w1 i1 j1 k1) (SQPF w2 i2 j2 k2) = SQPF (w1+w2) (i1+i2) (j1+j2) (k1+k2)

mul :: SplitQPF -> SplitQPF -> SplitQPF
mul (SQPF w1 i1 j1 k1) (SQPF w2 i2 j2 k2) = SQPF w i j k
 where

  w = w1*w2 - i1*i2 + j1*j2 + k1*k2
  i = w1*i2 + i1*w2 + j1*k2 - k1*j2
  j = w1*j2 + i1*k2 + j1*w2 - k1*i2
  k = w1*k2 - i1*j2 + j1*i2 + k1*w2

normM p a = snd $ a `divMod` p

-- pMul :: SCIPrime -> SplitCPF -> SplitCPF -> SplitCPF
pMul p a b = normM p $ mul (normM p a) (normM p b)

divMod :: SplitQPF -> SplitQPF -> (SplitQPF, SplitQPF)
divMod (SQPF w1 i1 j1 k1) (SQPF w2 i2 j2 k2) = (SQPF w3 i3 j3 k3, SQPF w4 i4 j4 k4)
 where
  w =   w1*w2  + i1*i2 - j1*j2 - k1*k2
  i = (-w1*i2) + i1*w2 - j1*k2 + k1*j2
  j = (-w1*j2) - i1*k2 + j1*w2 + k1*i2
  k = (-w1*k2) + i1*j2 - j1*i2 + k1*w2
  p = w2*w2 + i2*i2 - j2*j2 - k2*k2
  (w3,w4) = P.divMod w p
  (i3,i4) = P.divMod i p
  (j3,j4) = P.divMod j p
  (k3,k4) = P.divMod k p
  -- divMod a b = (a,b)

-}

{-

  *                                       *                                       *                                       *
w1*w2 + w1*i2 + w1*j2 + w1*k2 + i1*w2 + i1*i2 + i1*j2 + i1*k2 + j1*w2 + j1*i2 + j1*j2 + j1*k2 + k1*w2 + k1*i2 + k1*j2 + k1*k2
w       i       j       k       i       -w      -k      j       j       k       w       i       k       -j      -i      w
+       -       -       -       +        -       -      -       +       -       -       -       +        -       -      -
w      -i      -j      -k       i        w       k     -j       j      -k      -w      -i       k        j       i     -w
        A                       A
                B                                               B
                        C                                                                       C
                                                 D                      D
                                                        E                                                E
                                                                                        F                        F
                           


(w1 + i1 + j1 + k1) * (+ w2 - i2 - j2 - k2)
------------------------------------------
num(w2*w2 + i2*i2 - j2*j2 - k2*k2)


-}

randomSplitCI :: IO SplitQPF
randomSplitCI = do
  let mx = 2^70
  a <- randomRIO (-mx,mx :: Integer)
  b <- randomRIO (-mx,mx :: Integer)
  c <- randomRIO (-mx,mx :: Integer)
  d <- randomRIO (-mx,mx :: Integer)
  return $ SQPF a b c d

{-

-}
-}
  


